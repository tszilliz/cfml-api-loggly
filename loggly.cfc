component output=true accessors=true {

	// Replace the customerToken with your own data
	// Please note: don't confuse the customerToken with the API token in Loggly. You need the customerToken
	// The string 'customerToken' in the urlEndPointLoggly is automatically replaced with the actual customerToken
	static {
		customerToken = "[[YourCustomerToken]]";
		account = "[[YourAccountname]]";
		username = "[[YourUsername]]";
		password = "[[YourPassword]]";
		urlEndPointSend = "https://logs-01.loggly.com/inputs/customerToken/tag/http/";
		urlEndPointAPI = "https://account.loggly.com/apiv2/";
	}

	// This public function sends the logfile to Loggly and returns the resulting statuscode (hopefully '200 OK')
	public string function sendLogLine(required string logData) {
		if (!isJSON(arguments.logData)) {
			throw("Log data is not in JSON string notation");
		} else {
			httpService = new http(method="POST", charset="utf-8", url="#this.convertUrlEndPointSend(static.urlEndPointSend)#");
			httpService.addParam(name="content-type", type="header", value="application/x-www-form-urlencoded");
			httpService.addParam(name="Accept", type="header", value="application/x-www-form-urlencoded");
			httpService.addParam(encoded="false", type="body", value="#trim(arguments.logData)#");
			var result = httpService.send().getPrefix();
			return result.statuscode;
		}
	}

	public any function receiveLogLines(required string q, string from = "-24h", string until = "now", string order = "desc", string size = "50") {
		// Loggly is asked to make a search order, for which we'll get an RSID
		var logglyRsid = this.getLogglyResultset("#this.urlEndPointAPI(static.urlEndPointAPI)#search?q=#arguments.q#&from=#arguments.from#&until=#arguments.until#&order=#arguments.order#&size=#arguments.size#");
		var logglyRsidDes = DeserializeJSON(logglyRsid);
		// Loggly is asked to give the resulting resultset using the RSID as an identifier
		var resultSet = this.getLogglyResultset("#this.urlEndPointAPI(static.urlEndPointAPI)#events?rsid=#logglyRsidDes.rsid.id#");
		return resultSet;
	}

	// This private function calls the Loggly API
	private any function getLogglyResultset(required string urlEndPoint) {
		httpService = new http(method="GET", charset="utf-8", username="#static.username#", password="#static.password#", url="#arguments.urlEndPoint#");
		httpService.addParam(name="content-type", type="header", value="application/x-www-form-urlencoded");
		httpService.addParam(name="Accept", type="header", value="application/x-www-form-urlencoded");
		httpService.addParam(name="Referer", type="header", value="#cgi.http_referer#");
		return httpService.send().getPrefix().filecontent;
	}

	// This private function converts the urlEndPointSend URL so that it also contains your actual customerToken
	private string function convertUrlEndPointSend(required string urlEndPoint) {
		return replaceNoCase(arguments.urlEndPoint,"customerToken",static.customerToken);
	}

	// This private function converts the URL so that it also contains your actual account name
	private string function urlEndPointAPI(required string urlEndPoint) {
		return replaceNoCase(arguments.urlEndPoint,"account",static.account);
	}

}